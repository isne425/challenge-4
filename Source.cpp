#include<iostream>
#include<vector>
#include<cstdlib>
#include<ctime>
#include<cmath>
#include<iomanip>

using namespace std;

void display(vector<int> v, int size);
float sum(vector<int> v, int size);
void bubbleSort(vector<int> &v, int size);
float min(vector<int> v, int size);
float max(vector<int> v, int size);
float mean(vector<int> v, int size);
float med(vector<int> v, int size);
float mod(vector<int> v, int size);
float sd(vector<int> v, int size);
int even(vector<int> v, int size);
int odd(vector<int> v, int size);


void main()
{
	srand(time(0));

	int  size = rand() % 100 + 50; // random size of vector [50-150]
	vector<int> v; // vector declaration

	for (int i = 0; i<size; i++) // random each element
	{
		v.push_back(rand() % 100 + 1.0); // random value of element [1-100]
	}
	display(v, size);
	cout << endl  ;

	int input; // store what function user want to use
	do
	{
		cout << endl;
		cout << "1)The number of elements.\n";
		cout << "2)The sum of all elements.\n";
		cout << "3)The highest value.\n";
		cout << "4)The lowest value.\n";
		cout << "5)The mean value.\n";
		cout << "6)The median value.\n";
		cout << "7)The mode value.\n";
		cout << "8)The standard deviation.\n";
		cout << "9)The number of even numbers.\n";
		cout << "10)The number of odd numbers.\n";
		cout << "11)The values output in order from lowest to highest.\n";
		cout << "\nWhat do you want to know? [0 to end]: ";
		cin >> input;

		switch (input)
		{
			case 1 :
				cout << "The number of elements is " << size << endl;
				break;
			case 2 :
				cout << "The sum of all elements is " << sum(v,size) << endl;
				break;
			case 3 :
				cout << "The highest value is " << max(v, size) << endl;
				break;
			case 4 :
				cout << "The lowest value is " << min(v, size) << endl;
				break;
			case 5 :
				cout << "The mean value is " << mean(v, size) << endl;
				break;
			case 6 :
				cout << "The median value is " << med(v, size) << endl;
				break;
			case 7 :
				cout << "The mode value is " << mod(v, size) << endl;
				break;
			case 8 :
				cout << "The standard deviation is " << sd(v, size) << endl;
				break;
			case 9 :
				cout << "The number of even numbers is "<< even(v, size) << endl;
				break;
			case 10 :
				cout << "The number of odd numbers is " << odd(v, size) << endl;
				break;
			case 11 :
				cout << "The values output in order from lowest to highest : "  << endl;
				bubbleSort(v, size);
				display(v, size);
				cout << endl;
				break;
			default :
				cout << "Invalid input. Please try again." << endl;
				break;
		}
		cout << "==========================================================" << endl;
	} while (input != 0); // input 0 = end
	

}


void display(vector<int> v, int size) // O(n)
{
	for (int i = 0; i < size; i++) 
	{
		if (i % 10 == 0) { cout << endl; } 
		cout <<setw(5) << v[i] ; 
	}
}


float sum(vector<int> v, int size) // O(n)
{
	float sum = 0.0;
	for (int i = 0; i < size; i++) 
	{
		sum = sum + v[i];
	}
	return sum;
}

void bubbleSort(vector<int> &v, int size) // O(n^2)
{
	float swap, holder;
	do 
	{ 
		swap = 0;
		for (int j = 0; j<size - 1; j++) 
		{ 
			if (v[j]>v[j + 1]) 
			{
				holder = v[j];
				v[j] = v[j + 1];
				v[j + 1] = holder;
				swap = 1;
			}
		}
	} while (swap == 1);
}

float min(vector<int> v, int size) // O(n)
{
	float min=v[0];
	for (int i = 1; i < size; i++)
	{
		if (v[i] < min) { min = v[i]; }
	}
		
	return min;
}

float max(vector<int> v, int size) // O(n)
{
	float max = v[0];
	for (int i = 1; i < size; i++)
	{
		if (v[i] > max) { max = v[i]; }
	}

	return max;
}

float mean(vector<int> v, int size) // O(1)
{
	return sum(v, size) / size;
}

float med(vector<int> v, int size) // O(1)
{
	float med;
	bubbleSort(v, size);
	if (size % 2 == 0) { med = (v[size / 2] + v[(size / 2) + 1]) / 2; } // size is even  number
	else { med = v[size / 2]; } // size is odd number
	return med;
}

float mod(vector<int> v, int size)  //O(n^2)
{
	float value = 0.0, mode = 0.0;
	for (int i = 0; i<size; i++) 
	{
		float count = 0;
		for (int j = 0; j<size; j++) 
		{
			if (v[j] == v[i]) {count++;}	
			if (count>mode) // there is more another repeat number than previous 
			{
				mode = count; // repeat times = mode
				value = v[i];
			}
		}
	}
	return value;
}

float sd(vector<int> v, int size) //O(n)
{
	float sd=0.0;
	for (int i = 0; i <size; i++)
	{
		sd += pow(v[i] - mean(v, size), 2);
	}
	return sqrt(sd / size);
}

int even(vector<int> v, int size) //O(n)
{
	int count = 0;
	for (int i = 0; i < size; i++)
	{
		if (v[i] % 2 == 0) { count++ ; }
	}
	return count;
}

int odd(vector<int> v, int size) //O(n)
{
	int count = 0;
	for (int i = 0; i < size; i++)
	{
		if (v[i] % 2 == 1) { count++; }
	}
	return count;
}

